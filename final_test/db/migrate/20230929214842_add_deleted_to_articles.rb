class AddDeletedToArticles < ActiveRecord::Migration[7.0]
  def change
    add_column :articles, :deleted, :boolean, default: false
  end
end
