class CreateVotingCounts < ActiveRecord::Migration[7.0]
  def change
    create_table :voting_counts do |t|
      t.references :comment, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.boolean :voted, default: false

      t.timestamps
    end
  end
end
