UserRole.destroy_all
Comment.destroy_all
Article.destroy_all
User.destroy_all
Role.destroy_all

Role.create(role_name: 'admin')
Role.create(role_name: 'user')

category = Category.create(name: 'test')
category2 = Category.create(name: 'test2')
category3 = Category.create(name: 'test3')

10000.times do
  u = User.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email,
                   password: 'password', password_confirmation: 'password', confirmed_at: Time.current)
  user = User.find(u.id)
  role = Role.find_by(role_name: %w[admin user].sample)
  UserRole.create(user: user, role: role)
  2.times do
    a = Article.create(title: Faker::Lorem.words(number: 5), description: Faker::Lorem.sentences(number: 2),
                       content: Faker::Lorem.paragraph, user: u, category: [category, category2, category3].sample, published: true)
    2.times do
      Comment.create(value: rand(1..10), contents: Faker::Lorem.sentence, user: u, article: a)
    end
  end
end
