class CategoryController < ApplicationController

    def index
        @pagy, @categories = pagy(Category.all, items: params[:per_page])
    
        render json: @categories
    end

    def show
        if params[:per_page]
            items = params[:per_page]
        else
            items = 10
        end

        @category = Category.find(params[:id])
        
        @pagy, @articles = pagy(Article.all, items: items).where.not(deleted: true).where.not(published: false)
        render json: { "category": @category, "article": ActiveModel::SerializableResource.new(@articles) }
    end
end
