# frozen_string_literal: true

class ArticlesController < ApplicationController
  before_action :authenticate_user!

  def get_versions
    @article = Article.find_by(id: params[:id])
    authorize! :get_versions, @article
    @versions = @article.versions

    render json: @versions, status: :ok
  end

  def restore_previous_version
    @article = Article.find_by(id: params[:id])
    authorize! :restore_previous_version, @article
    @version = @article.versions.find(params[:version_id])
    @article = @version.reify
    @article.save

    render json: "Article has been reverted to a previous version", status: :ok
  end

  def set_cover
    @article = Article.find_by(id: params[:id])
    authorize :set_cover, @article
    @article.update(cover: params[:cover])

    render json: 'Article cover updated', status: :ok
  end

  def create
    @article = Article.create(title: params[:title], description: params[:description], content: params[:content],
                              user: current_user, category: Category.find_by(id: 1), deleted: false)
    tags = params[:tags]
    parsed_tags = tags.split(',')
    parsed_tags.each do |tag|
      created_tag = Tag.find_or_create_by(name: tag.downcase.strip)
      ArticleTag.create(article_id: @article.id, tag_id: created_tag.id)
    end

    render json: 'Article has been created', status: :ok
  end

  def publish
    @article = Article.find(params[:id])
    authorize! :publish, @article

    if @article.published == false
      @article.update(published: true)
      render json: 'This article is now public.', status: ok
    else
      render json: 'This article is already public!', status: :error
    end
  end

  def hide
    @article = Article.find(params[:id])
    authorize! :hide, @article

    if @article.published == true
      @article.update(published: false)
      render json: 'This article is now hidden.', status: :ok
    else
      render json: 'This article is already hidden!', status: :error
    end
  end

  def index

    if params[:per_page]
      items = params[:per_page]
    else
      items = 10
    end

    @pagy, @articles = pagy(Article.where(published: true).where(deleted: false), items: items)

    render json: @articles
  end

  def show

    if params[:comments_per_page]
      items = params[:comments_per_page]
    else
      items = 10
    end

    @article = Article.find(params[:id])
    raise NotFound, "Can't find the article you are looking for" if @article.deleted?

    @pagy, @comments = pagy(Comment.where(article_id: params[:id]).order(value: :desc),
                            items: items)
    authorize! :see_hidden, @article if @article.published == false
    render json: { "article": ActiveModel::SerializableResource.new(@article),
                   "comments": ActiveModel::SerializableResource.new(@comments) }
  end

  def update
    @article = Article.find(@article_id)
    authorize! :update, @article
    @article.update(@update_params)
    
    render json: 'Article updated', status: :ok
  end

  def destroy
    @article = Article.find(params[:id])
    authorize! :destroy, @article
    @article.update(deleted: true)

    render json: "Article has been deleted"
  end

  def search
    @article = Article.where('title like ?', "%#{params[:query]}%").where.not(published: false).where.not(deleted: true)

    render json: { "search_results": ActiveModel::SerializableResource.new(@article) }
  end

  private

  def update_params
    allowed_params = params.require(:article).permit(:title, :description, :content)
    allowed_params.each do |key, value|
      allowed_params[key] = value.presence || article.send(key)
    end
    allowed_params
  end
end
