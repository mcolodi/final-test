# frozen_string_literal: true

class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create
    build_resource(sign_up_params)
    resource.save
    
    if resource.persisted?
      user = User.find(resource.id)
      role = Role.find_by(role_name: 'user')
      UserRole.create(user: user, role: role)
      render json: { message: 'User registered successfully.' }, status: :created
    else
      render json: { error: resource.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def sign_up_params
    return unless params.require(:user).present?

    params.require(:user).permit(:email, :password, :password_confirmation, :last_name, :first_name, :city, :country,
                                 :age)
  end
end
