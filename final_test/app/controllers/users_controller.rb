# frozen_string_literal: true

class UsersController < ApplicationController
    before_action :authenticate_user!
    include ActiveStorage::SetCurrent
    
    def set_avatar
      @user = User.find_by(id: params[:id])
      @user.update(avatar: params[:avatar])
    end

    def ban 
      @user = User.find(params[:id])
      authorize! :ban, @user
      @user.update(banned: true)

      render json: "User with ID #{@user.id} has been banned"
    end

    def unban 
      @user = User.find(params[:id])
      authorize! :ban, @user
      @user.update(banned: false)

      render json: "User with ID #{@user.id} has been unbanned"
    end

    def index
      @pagy, @users = pagy(User.all, items: params[:per_page])
      authorize! :index, @users

      render json: @users
    end
  
    def show
      @user = User.find(params[:id])
      authorize! :show, @user
      render json: @user
    end
  
    def update
      @user = User.find(params[:id])
      authorize! :update, @user
      @user.update(update_params)
  
      json_string = UserSerializer.new(@user).serializable_hash.to_json
      render json: json_string
    end
  
    def destroy
      @user = User.find(params[:id])
      authorize! :destroy, @user
      @user.destroy
  
      render json: "#{@user.first_name} has been deleted"
    end
  
    private
  
    def update_params
      allowed_params = params.require(:user).permit(:first_name, :last_name, :email, :city, :country, :age)
      allowed_params.each do |key, value|
        allowed_params[key] = value.presence || user.send(key)
      end
      allowed_params
    end
  end