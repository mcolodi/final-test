class CommentsController < ApplicationController
    before_action :authenticate_user!

    def index
        @comments = Comment.where(article_id: params[:id]).order(value: :desc)
        
        render json: {"comments": ActiveModel::SerializableResource.new(@comments), status: :ok}
    end

    def create
        @comment = Comment.create(contents: params[:contents], user_id: current_user.id, article_id: params[:id], value: 0)
    end

    def update
        @comment = Comment.find_by(id: params[:comment_id])
        authorize! :update, @comment
        @comment.update(update_params)

        render json: @comment
    end

    def destroy
        @comment = Comment.find_by(id: params[:comment_id])
        authorize! :destroy, @comment
        @comment.destroy

        render json: {message: "Comment has been deleted", status: :ok}
    end

    def upvote
        @comment = Comment.find_by(id: params[:comment_id])
        @user = current_user
        if @comment.voted?(@user.id, @comment.id) == false
            @comment.update(value: @comment.value + 1)
            @comment.change_voted_field(@user.id, @comment.id)
        elsif  @comment.voted?(@user.id, @comment.id) == true
            raise RuntimeError, "You can't vote twice"
        end
    end

    def downvote
        @comment = Comment.find_by(id: params[:comment_id])
        @user = current_user
        if @comment.voted?(@user.id, @comment.id) == false
            @comment.update(value: @comment.value - 1)
            @comment.change_voted_field(@user.id, @comment.id)
        elsif  @comment.voted?(@user.id, @comment.id) == true
            raise RuntimeError, "You can't vote twice"
        end
    end

    private 

    def update_params
        allowed_params = params.require(:comment).permit(:content)
        allowed_params.each do |key, value|
            allowed_params[key] = value.presence || article.send(key)
        end
        allowed_params
    end
end
