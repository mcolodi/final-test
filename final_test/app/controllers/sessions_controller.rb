# frozen_string_literal: true

class SessionsController < Devise::SessionsController
  respond_to :json

  def create
    user = User.find_by(email: sign_in_params[:email])
    if user&.valid_password?(sign_in_params[:password])
      sign_in(user)
      render json: { message: 'Logged in successfully.' }
    else
      render json: { error: 'Invalid email or password.' }, status: :unauthorized
    end
  end

  private

  def sign_in_params
    return unless params.require(:user).present?

    params.require(:user).permit(:email, :password)
  end
end
