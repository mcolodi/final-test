class ApplicationController < ActionController::API
    include Pagy::Backend
    include ActionController::MimeResponds
    include ActiveStorage::SetCurrent
end
