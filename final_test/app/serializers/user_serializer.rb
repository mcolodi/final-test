class UserSerializer < ActiveModel::Serializer
  attributes :first_name, :last_name, :latest_article, :avatar

  def latest_article
    article = Article.find_by(user_id: object.id)
    article&.title
  end

  def avatar
    object.avatar.url
  end
end