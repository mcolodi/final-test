class PaperTrailSerializer < ActiveModel::Serializer
  def self.load(data)
    JSON.parse(data)
  end

  def self.dump(data)
    JSON.dump(data)
  end
end