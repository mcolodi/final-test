class CommentSerializer < ActiveModel::Serializer
  attributes :value, :contents, :created_at, :updated_at, :author

  def author
    u = User.find_by(id: object.user_id)
    u.first_name + " " + u.last_name
  end
end
