class ArticleSerializer < ActiveModel::Serializer
  include Pagy::Backend
  attributes :title, :description, :content, :created_at, :tags, :category, :author, :cover

  def tags
    tags = ArticleTag.where(article_id: object.id)
    to_return = []
    tags.each do |tag|
      to_return.push(tag.tag.name)
    end
    return to_return
  end

  def category
    object.category.name
  end

  def author
    object.user.first_name + ' ' + object.user.last_name
  end

  def cover
    object.cover.url
  end
end