class Comment < ApplicationRecord
    belongs_to :user
    belongs_to :article

    validates :value, presence: true 
    validates :contents, presence: true 

    def voted?(user_id, comment_id)
        instance = VotingCount.find_or_create_by(user_id: user_id, comment_id: comment_id)
        if instance.voted == true
            return true
        else
            return false
        end
    end

    def change_voted_field(user_id, comment_id)
        VotingCount.find_or_create_by(user_id: user_id, comment_id: comment_id)
        VotingCount.update(voted: true)
    end
end
