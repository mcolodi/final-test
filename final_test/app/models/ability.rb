# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user.has_role?('admin')
      can :manage, :all
      can :ban, User
      can :unban, User
    elsif user.has_role?('user')
      can :manage, User, id: user.id
      cannot :index, User
      cannot :ban, User
      cannot :unban, User

      can :index, Article
      can :update, Article, user_id: user.id
      can :update, Comment, user_id: user.id
      can :see_hidden, Article, user_id: user.id
      can :get_version, Article, user_id: user.id
      can :restore_previous_version, Article, user_id: user.id
      can :set_cover, Article, user_id: user.id
      can :publish, Article, user_id: user.id
      can :hide, Article, user_id: user.id
      can :destroy, Article, user_id: user.id
      can :show, Article, user_id: user.id

      can :manage, Comment, user_id: user.id
      

    end
  end
end
