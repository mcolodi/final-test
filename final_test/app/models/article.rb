class Article < ApplicationRecord
    has_paper_trail limit: 5
    has_one_attached :cover
    belongs_to :user 
    belongs_to :category
    has_many :tags, through: :article_tags
    has_many :comments
    accepts_nested_attributes_for :tags
    validates :title, presence: true

    def deleted?
        deleted == true
    end
end
