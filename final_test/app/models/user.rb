class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :validatable, :jwt_authenticatable, jwt_revocation_strategy: JwtDenylist#,:confirmable
    has_one_attached :avatar
    has_many :articles
    has_many :comments
    has_many :user_roles
    has_many :roles, through: :user_roles, dependent: :destroy

    validates :first_name, presence: true
    validates :last_name, presence: true
    
    def has_role?(role_name)
      roles.exists?(role_name: role_name)
    end

    def active_for_authentication?
      super && !banned?
    end
end
