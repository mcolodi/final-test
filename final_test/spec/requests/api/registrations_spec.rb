RSpec.describe 'auth/', type: :request do

    describe 'devise-related authentication requests' do
        
        context 'User parameters on registration are valid' do
          valid_params = FactoryBot.attributes_for(:user)
          it 'Creates the user and returnes a success message' do
            post '/auth/sign_up',
                 params: { "user": valid_params }
    
            response_json = JSON.parse(response.body)
            expect(response_json['message']).to eq('User registered successfully.')
          end
        end
    
        context 'User parameters on registration are invalid' do
          invalid_params = FactoryBot.attributes_for(:user, email: 'ThisEmailIsInvalid')
          it 'It does not create a new user and returns an error message' do
            post '/auth/sign_up',
                 params: { "user": invalid_params }
            response_json = JSON.parse(response.body)
            expect(response_json['message']).to_not eq('User registered successfully.')
          end
        end
        
        context 'Registration parameters are valid, but json request is badly formatted (no user key)' do
          valid_params = FactoryBot.attributes_for(:user)
          it 'Raises parameter missing exception' do
            expect { post '/auth/sign_up', params: valid_params }.to raise_error(ActionController::ParameterMissing)
          end
        end
    
        context 'Login parameters on login are valid' do
          it 'Logs the user in, returns success message' do
            valid_params = FactoryBot.attributes_for(:user)
            User.create!(valid_params)
            post '/auth/login', params: { "user": { "email": valid_params[:email], "password": valid_params[:password] } }
            response_json = JSON.parse(response.body)
            expect(response_json['message']).to eq('Logged in successfully.')
          end
        end
    
        context 'Login parameters on login are invalid' do
          it 'Does not log in, returns error message' do
            valid_params = FactoryBot.attributes_for(:user)
            User.create!(valid_params)
            post '/auth/login', params: { "user": { "email": valid_params[:email], "password": 'ThisPasswordIsWrong' } }
            response_json = JSON.parse(response.body)
            expect(response_json['error']).to eq('Invalid email or password.')
          end
        end
    
        context 'Sending a log out request' do
          it 'logs the user out' do
            #Logging in 
            valid_params = FactoryBot.attributes_for(:user)
            User.create!(valid_params)
            post '/auth/login', params: { "user": { "email": valid_params[:email], "password": valid_params[:password] } }
            
            delete '/auth/sign_out'
            expect(response).to have_http_status(204)
          end
        end
      end
    end
    