# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Users', type: :request do
  include Devise::Test::IntegrationHelpers
  let(:user) { FactoryBot.create(:user) }
  let(:different_user) { FactoryBot.create(:user) }

  describe 'CanCan admin/user roles testing' do
    context 'User is logged as admin and is trying to access #index action' do
      it 'Allows the user to access the resource' do
        sign_in user
        test_role = Role.create(role_name: 'admin')
        UserRole.create(user: user, role: test_role)
        get users_path
        expect(response).to have_http_status(:success)
      end
    end

    context 'User is not logged in and is accessing the resource' do
      it 'returns 401 status code' do
        get users_path
        expect(response).to have_http_status(401)
      end
    end

    context 'Admin bans a user' do
      it 'grants the admin access to do so' do
        sign_in user
        test_role = Role.create(role_name: 'admin')
        UserRole.create(user: user, role: test_role)
        delete "/users/manage/#{different_user.id}"
        expect(response).to have_http_status(200)
      end

      it 'revokes the banned user access to app' do
        sign_in different_user
        expect{ get "/users/#{different_user.id}" }.to raise_error(CanCan::AccessDenied)
      end        
    end

    context 'User is not logged as an admin and is trying to access #index action' do
      it 'Throws access denied exception' do
        sign_in user
        expect { get users_path }.to raise_error(CanCan::AccessDenied)
      end
    end

    context 'User is not logged as admin and is trying to access a CRUD action on their own user id' do
      it 'Allows any CRUD action' do
        sign_in user
        test_role = Role.create(role_name: 'user')
        UserRole.create(user: user, role: test_role)
        get "/users/#{user.id}"
        response_json = JSON.parse(response.body)
        expect(response_json['first_name']).to eq(user.first_name.to_s)
      end
    end
    
    context 'User is not logged in as admin and is trying to access a CRUD action on a different user id' do
      it 'raises access denied exception' do
        sign_in user
        test_role = Role.create(role_name: 'user')
        UserRole.create(user: user, role: test_role)
        expect { get "/users/#{different_user.id}" }.to raise_error(CanCan::AccessDenied)
      end
    end
  end

  describe 'Basic user controller actions' do
    context 'Update action' do
      it 'Updates the requested value' do
        sign_in user
        test_role = Role.create(role_name: 'user')
        UserRole.create(user: user, role: test_role)
        patch "/users/manage/#{user.id}/edit", params: { "user": { "first_name": 'updated value' } }
        response_json = JSON.parse(response.body)
        expect(response_json['first_name']).to eq('updated value')
      end
    end

    context 'Delete action' do
      it 'Deletes the user' do
        sign_in user
        test_role = Role.create(role_name: 'user')
        UserRole.create(user: user, role: test_role)
        delete "/users/#{user.id}"
        expect(response.body).to eq("#{user.first_name} has been deleted")
      end
    end
  end
end
