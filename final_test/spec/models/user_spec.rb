# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  subject do
    User.create!(first_name: 'John', last_name: 'Ceena',
                 email: 'example1@example.com', password: 'password', password_confirmation: 'password')
  end

  it 'is valid with valid attrs' do
    expect(subject).to be_valid
  end

  it 'is not valid when any of attrs is missing' do
    subject.first_name = nil
    expect(subject).to_not be_valid
  end

  it 'is not valid when email formatting is wrong' do
    subject.email = 'notcontainingadomain'
    expect(subject).to_not be_valid
  end
  
  it 'is not valid when password is shorter than 6' do
    subject.password = 'four'
    expect(subject).to_not be_valid
  end
end
