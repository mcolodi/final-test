# FactoryBot test user params

FactoryBot.define do
    factory :user do
      email { Faker::Internet.email }
      password { "password" }
      password_confirmation { "password" }
      last_name { Faker::Name.last_name }
      first_name { Faker::Name.first_name }
    end
  end