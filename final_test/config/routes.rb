Rails.application.routes.draw do
  resources :users
  devise_for :users, path: 'auth', path_names: {
    sign_in: 'login', sign_out: 'sign_out', registration: 'sign_up'
  },
  controllers: { sessions: 'sessions', registrations: 'registrations'}

  get 'users/', to: 'users#index'
  get 'users/:id/profile', to: 'users#show'
  delete 'users/manage/:id', to: "users#ban"
  post 'users/manage/:id', to: "users#unban"
  patch 'users/manage/:id/edit', to: 'users#update'
  post 'users/:id/avatar', to: 'users#set_avatar'

  post 'articles/manage/:id/versions/revert', to: 'articles#restore_previous_version'
  get 'articles/manage/:id/versions', to: 'articles#get_versions'
  post 'articles/manage/:id/set_cover', to: 'articles#set_cover'
  get 'articles/view', to: 'articles#index'
  patch 'articles/manage/:id/edit', to: 'articles#update'
  get 'articles/view/:id', to: 'articles#show'
  post 'articles/manage/:id/publish', to: 'articles#publish'
  delete 'articles/manage/:id/hide', to: 'articles#hide'
  post '/articles/new', to: 'articles#create'
  delete 'articles/manage/:id/delete', to: 'articles#destroy'
  post 'articles/search', to: 'articles#search'

  post 'articles/view/:id/comments/new_comment', to: 'comments#create'
  get 'articles/view/:id/comments', to: 'comments#index'
  patch 'articles/view/:id/comments/:comment_id/edit', to: 'comments#update'
  post 'articles/view/:id/comments/:comment_id/up', to: 'comments#upvote'
  delete 'articles/view/:id/comments/:comment_id/delete', to: 'comments#destroy'
  delete 'articles/view/:id/comments/:comment_id/down', to: 'comments#downvote'

  get 'categories/view', to: 'category#index'
  get 'categories/view/:id', to: 'category#show'


  mount Rswag::Ui::Engine => "/api-docs"
  mount Rswag::Api::Engine => "/api-docs"
end
