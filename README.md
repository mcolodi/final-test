1. Setting the project up

Execute the following commands:

    ``docker-compose up``
    ``docker-compose exec app rails db:create db:migrate db:seed``

Next set up master key for rails:

    ``docker-compose exec app bash``
    Inside the container:
    ``export EDITOR=code``
    ``rails credentials:edit``

Restart the docker-compose. 

2. Rspec

Execute the following commands:

    ``docker-compose exec app rails db:test:prepare``
    ``docker-compose exec app rspec``

The Rspec tests cover the User model, users_controller, and authentication procedures.

What is done: 
- set avatar/cover from active storage, locally
- articles can be published/hidden, visible only to author
- unique tags are being created on article creation
- default devise html email templates/password recovery
- article search by title, default 10 items pagination, settable by request param
- comments in article default 25 pagination, settable by request param
- banning/unbanning
- article version control and reverting to selected version
- article soft delete
- comments upvoting/downvoting
What is not done:
- Rspec coverage 90%
- Swagger
- Email confirmation on registration/sidekiq 
- Business logic isolation
