# Use a base image with Ruby installed
FROM ruby:3.2.2

# Set the working directory inside the container
WORKDIR /app

# Copy the Gemfile and Gemfile.lock to the container
COPY final_test/Gemfile final_test/Gemfile.lock ./

# Install dependencies
RUN bundle install

# Copy the rest of the application code to the container
COPY ./final_test/ .
WORKDIR /app/final_test
